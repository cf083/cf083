
#include <stdio.h>
#include <math.h>

int main()
{
    int a,b,c,d;
    float r1,r2,im1,im2;
    
    printf("Enter the values for a,b and c(ax^2+bx+c):\n");
    scanf("%d%d%d",&a,&b,&c);
    
    d=(b*b)-(4*a*c);
    
    if(d>0)
    {
    r1=(-b+sqrt(d))/(2*a);
    r2=(-b-sqrt(d))/(2*a);
    printf("The roots are real and distinct.\n");
    printf("first root= %f\n",r1);
    printf("second root= %f\n",r2);
    }
    
    else if(d<0)
    {
    im1=(sqrt(-d))/(2*a);
    im2=(sqrt(-d))/(2*a);
    r1=(-b)/(2*a);
    r2=(-b)/(2*a);
    printf("The roots are imaginary.\n");
    printf("first root= %f+%fi\n",r1,im1);
    printf("second root= %f-%fi\n",r2,im2);
    }
    
    else if(d==0)
    {
    r1=(-b)/(2*a);
    r2=r1;
    printf("The roots are real and equal.\n");
    printf("first root= %f\n",r1);
    printf("second root= %f\n",r2);
    }
    return 0;
}