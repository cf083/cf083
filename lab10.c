#include <stdio.h>
#include <stdlib.h>

int main()
{
 FILE *fp;
 char ch;
 fp=fopen("input.txt","w");
 printf("enter date..\n");
 while((ch=getchar())!=EOF)
 {
  putc(ch,fp);
 }
 fclose(fp);
 fp=fopen("input.txt","r");
 printf("file contents are:\n");
 while((ch=getc(fp))!=EOF)
 {
  printf("%c",ch);
 }
 fclose(fp);
 return 0;
}
